import express from "express";
import mongoose from "mongoose";
import bodyParser from "body-parser";
import router from "./api/routes/api/routes";
import cors from "cors";
import Key from "./api/config/config";

const app = express();

//boddy parser middleware
app.use(bodyParser.json());

mongoose.connect(
	Key.mongoURI,
	{ useNewUrlParser: true, useUnifiedTopology: true },
	() => {
		console.log("databse connected2");
	},
);

app.use(express.json());
app.use(cors());
app.use("/app", router);
const port = process.env.PORT || 5000;
app.listen(port, () => {
	console.log(`server runing on port ${port}`);
});
