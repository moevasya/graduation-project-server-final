import express, { response } from "express";
import Info from "../../models/sights";
import mongoose from "mongoose";
import weatherData from "../../utils/weather";
import newsData from "../../utils/news";

const router = express.Router();

const db = mongoose.connection;

router.post("/post", (req, resp) => {
	const newSight = new Info({
		name: req.body.name,
		description: req.body.description,
		image: req.body.image,
	});
	resp.setHeader("Cache-Control", "s-max-age=1, stale-while-revalidate");

	newSight
		.save()
		.then((data) => {
			return resp.json(200);
		})
		.catch((err) => {
			resp.json("not signed up");
			console.log(err);
		});
});

router.post("/get", (req, resp) => {
	const query = new Info({
		name: req.body.name,
		description: req.body.description,
		image: req.body.image,
	});
	resp.setHeader("Cache-Control", "s-max-age=1, stale-while-revalidate");

	db.collection("sights").findOne(
		{ name: `${req.body.name}` },
		(err, result) => {
			if (err) {
				console.log("error");
			} else {
				resp.json(result);
			}
		},
	);
});

router.get("/Weather", (req, resp) => {
	const address: any = req.query.address;
	resp.setHeader("Cache-Control", "s-max-age=1, stale-while-revalidate");

	weatherData(address, (result: object) => {
		resp.json(result);
	});
});
router.get("/news", (req, resp) => {
	resp.setHeader("Cache-Control", "s-max-age=1, stale-while-revalidate");

	newsData((result: object) => {
		resp.json(result);
	});
});
export default router;
