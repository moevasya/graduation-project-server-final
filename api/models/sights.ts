import mongoose, { Schema } from "mongoose";

//Create Schema

const sightsTemplate = new mongoose.Schema(
  {
    name: {
      type: String,
      required: true,
      unique: true,
    },

    description: {
      type: String,
      required: true,
    },
    image: {
      type: String,
      required:true,
    },
  },
  { timestamps: true },
);

const Info = mongoose.model("sights", sightsTemplate);
export default Info;
