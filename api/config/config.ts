const moment = require("moment");
const Key = {
	mongoURI:
		"mongodb+srv://admin:admin@cluster0.j1lyh.mongodb.net/graduation-project?retryWrites=true&w=majority",
	weatherAPI: {
		BASE_URL: `http://api.openweathermap.org/data/2.5/forecast?q=`,
		API_KEY: `ca98617be22b7c8ea4a8d444241a7661`,
	},
	newsApi: {
		BASE_URL: `http://newsapi.org/v2/everything?q=apple&from=${moment(Date())
			.subtract(1, "day")
			.format("YYYY-MM-DD")}&to=${moment(Date()).format(
			"YYYY-MM-DD",
		)}&sortBy=popularity&apiKey=d3207a7c9ea04f0aa742b10d138f1a7e`,
	},
};
export default Key;
