import Keys from "../config/config";
import request from "request";

const newsData = ( callBack: any) => {
    const url =
        Keys.newsApi.BASE_URL;
        request({ url: url, json: true }, (error, { body }) => {
        if (error) throw error
        callBack(body)
    })
}

export default newsData;