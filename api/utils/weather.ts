import Keys from "../config/config";
import request from "request";

const weatherData = (address: string, callback: any) => {
  const url =
    Keys.weatherAPI.BASE_URL +
    encodeURIComponent(address) +
    "&appid=" +
    Keys.weatherAPI.API_KEY;
  request({ url:url, json:true },(error,{body})=>{
      if (error) throw error
      callback(body)
  });
};

export default weatherData;
